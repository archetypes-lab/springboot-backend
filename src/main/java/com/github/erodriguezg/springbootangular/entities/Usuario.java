package com.github.erodriguezg.springbootangular.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "usuario")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@ToString
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @EqualsAndHashCode.Include
    private Long idPersona;

    @NotBlank
    @Size(min = 4, max = 20)
    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @JsonProperty("perfil")
    @NotNull
    @JoinColumn(name = "id_perfil_usuario", referencedColumnName = "id_perfil_usuario")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private PerfilUsuario perfil;

    @NotNull
    @JoinColumn(name = "id_persona")
    @OneToOne(optional = false, fetch = FetchType.EAGER)
    @MapsId
    private Persona persona;

    @Column(name = "habilitado")
    private Boolean habilitado;

}
