package com.github.erodriguezg.springbootangular.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "provincia")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@ToString
public class Provincia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_provincia")
    @EqualsAndHashCode.Include
    private Integer id;

    @Column(name = "nombre")
    private String nombre;

    @ToString.Exclude
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_region")
    private Region region;

    public Provincia() {
    }

    public Provincia(Integer id) {
        this.id = id;
    }

}
