package com.github.erodriguezg.springbootangular.config;

import java.util.Properties;

import com.github.erodriguezg.springbootangular.utils.storage.StorageUtils;
import com.github.erodriguezg.springbootangular.utils.storage.s3.S3StorageUtils;

import org.jclouds.ContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class S3StorageConfig {

    private static final String PROVIDER = "s3";

    @Value("${app.storage.s3.bucket}")
    private String bucket;

    @Value("${app.storage.s3.endpoint:''}")
    private String endpoint;

    @Value("${app.storage.s3.access-key}")
    private String accessKey;

    @Value("${app.storage.s3.secret-key}")
    private String secretKey;

    @Bean
    public ContextBuilder contextBuilder() {
        Properties overrides = new Properties();
        if(!endpoint.isBlank()) {
            overrides.setProperty("jclouds.provider", PROVIDER);
            overrides.setProperty("jclouds.endpoint", this.endpoint);
        }
        return ContextBuilder.newBuilder(PROVIDER)
            .credentials(this.accessKey, this.secretKey)
            .overrides(overrides);
    }

    @Bean
    public StorageUtils s3StorageUtils(ContextBuilder contextBuilder) {
        return new S3StorageUtils(contextBuilder, this.bucket);
    }

}