package com.github.erodriguezg.springbootangular.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf()
            .and()
            .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "**")
                    .permitAll()
                .anyRequest()
                    .permitAll()
            .and()
                .exceptionHandling()
            .and()
                .headers()
                    .cacheControl()
                        .disable();
    }
}
