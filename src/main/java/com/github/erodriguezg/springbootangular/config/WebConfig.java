package com.github.erodriguezg.springbootangular.config;

import java.util.EnumSet;

import javax.servlet.DispatcherType;

import com.github.erodriguezg.springbootangular.security.CorsFilter;

import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    private static final Logger log = LoggerFactory.getLogger(WebConfig.class);

    @Value("${app.cors.allowed-origins}")
    private String corsAllowedOrigin;

    @Value("${spring.session.timeout:30m}")
    private String sessionTimeout;

    @Value("${server.servlet.session.cookie.same-site:None}")
    private String sameSiteCookie;

    @Value("${server.servlet.session.cookie.secure:false}")
    private Boolean secureCookie;

    /*
     * FileUpload
     */

    @Bean
    public CommonsMultipartResolver filterMultipartResolver() {
        CommonsMultipartResolver filterMultipartResolver = new CommonsMultipartResolver();
        filterMultipartResolver.setMaxUploadSize(20971520);
        return filterMultipartResolver;
    }

    /*
     * Filter CORS
     */

    @Bean
    public FilterRegistrationBean<CorsFilter> urlCorsFilter() {
        FilterRegistrationBean<CorsFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(new CorsFilter(this.corsAllowedOrigin));
        registration.addUrlPatterns("/*");
        registration.setDispatcherTypes(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST,
                DispatcherType.INCLUDE, DispatcherType.ASYNC, DispatcherType.ERROR));
        registration.setName("corsFilter");
        registration.setOrder(1);
        return registration;
    }

    /*
     * DEFAULT SESSION COOKIE
     */

    @Bean
    public CookieSerializer cookieSerializer() {
        DefaultCookieSerializer serializer = new DefaultCookieSerializer();
        serializer.setCookieName("THESESSION");
        serializer.setCookiePath("/");
        serializer.setSameSite(this.sameSiteCookie);
        serializer.setUseSecureCookie(this.secureCookie);
        serializer.setUseHttpOnlyCookie(true);
        serializer.setCookieMaxAge(calculateMaxAge());
        return serializer;
    }

    private int calculateMaxAge() {
        log.info("session timeout: {}", this.sessionTimeout);

        PeriodFormatter formatter = new PeriodFormatterBuilder()
            .appendDays().appendSuffix("d")
            .appendHours().appendSuffix("h")
            .appendMinutes().appendSuffix("m")
            .appendSeconds().appendSuffix("s")
            .toFormatter();

        Period period = formatter.parsePeriod(this.sessionTimeout);

        var seconds = period.toStandardSeconds().getSeconds();
        log.info("session timeout seconds: {}", seconds);
        return seconds;
    }

}
