package com.github.erodriguezg.springbootangular.config;

import com.github.erodriguezg.springbootangular.utils.templates.FreemarkerTemplateProcessor;
import com.github.erodriguezg.springbootangular.utils.templates.TemplateProcessor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TemplateProcessorConfig {

    @Bean
    public TemplateProcessor templateProcessor(freemarker.template.Configuration freemarkerConfig) {
        return new FreemarkerTemplateProcessor(freemarkerConfig);
    }

}
