package com.github.erodriguezg.springbootangular.config;

import com.github.erodriguezg.springbootangular.utils.pdfutils.PdfGenerator;
import com.github.erodriguezg.springbootangular.utils.pdfutils.flyingsaucer.FlyingSaucerGenerator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PdfGeneratorConfig {

    @Bean
    public PdfGenerator flyingSaucerGenerator() {
        return new FlyingSaucerGenerator();
    }

}
