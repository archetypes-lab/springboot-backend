package com.github.erodriguezg.springbootangular.rest;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.github.erodriguezg.springbootangular.dto.InfoDto;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/info")
public class InfoRest {

    @GetMapping
    public InfoDto getInfo(HttpServletRequest httpRequest) {
        var info = new InfoDto();
        info.setFechaPresente(new Date());
        info.setSesionTimeoutSegundos(httpRequest.getSession().getMaxInactiveInterval());
        return info;
    }

}
