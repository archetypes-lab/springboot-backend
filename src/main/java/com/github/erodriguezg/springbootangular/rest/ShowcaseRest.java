package com.github.erodriguezg.springbootangular.rest;

import javax.servlet.http.HttpServletResponse;

import com.github.erodriguezg.springbootangular.services.ShowcaseService;
import com.github.erodriguezg.springbootangular.services.exceptions.LogicaNegocioException;
import com.github.erodriguezg.springbootangular.utils.HttpUtils;
import com.github.erodriguezg.springbootangular.utils.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/showcase")
public class ShowcaseRest {

    private static final Logger log = LoggerFactory.getLogger(ShowcaseRest.class);

    @Autowired
    private ShowcaseService showcaseService;

    @Autowired
    private SecurityUtils securityUtils;

    @GetMapping("/error-negocio")
    public void lanzarErrorNegocio() {
        log.debug("--> lanzando error de negocio");
        throw new LogicaNegocioException("Hola, soy una excepción controlada en el flujo!");
    }

    @GetMapping("/error-interno")
    public void lanzarErrorInterno() {
        log.debug("--> lanzando error interno");
        throw new IllegalStateException("Hola, soy un bug!");
    }

    @GetMapping("/accion-admin")
    @PreAuthorize("hasAnyAuthority('Administrador')")
    public void accionAdministrador() {
        log.debug("--> si llegue aquí es porque tengo permiso de Administrador!");
    }

    @GetMapping("/sleep")
    public void sleep() {
        log.debug("-> comenzando (5 seg) zzZZzzzZzzzZZzzz");
        try {
            Thread.sleep((long) 1000 * 5);
            log.debug("-> Despertando!");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new IllegalStateException("ocurrio un error en el sleep", e);
        }
    }

    // manejo archivos

    @PostMapping("/generar-y-subir-archivo")
    @PreAuthorize("isAuthenticated()")
    public void generarYSubirArchivo(HttpServletResponse httpResponse) {
        var identidad = this.securityUtils.getIdentidadActual();
        var username = identidad.getUsername();
        this.showcaseService.generarYSubirAchivo(username);
        httpResponse.setStatus(HttpServletResponse.SC_OK);
    }

    @PostMapping("/eliminar-archivo")
    @PreAuthorize("isAuthenticated()")
    public void eliminarArchivo(HttpServletResponse httpResponse) {
        this.showcaseService.eliminarArchivo();
        httpResponse.setStatus(HttpServletResponse.SC_OK);
    }

    @PostMapping("/descargar-archivo")
    @PreAuthorize("isAuthenticated()")
    public void descargarArchivo(HttpServletResponse httpResponse) {
        var optional = this.showcaseService.obtenerArchivo();
        if (!optional.isPresent()) {
            throw new IllegalStateException("No existe el archivo");
        }
        var tempFile = optional.get();
        HttpUtils.downloadFile(httpResponse, tempFile, true);
    }

    // envio de emails

    @PostMapping("/enviar-email")
    @PreAuthorize("isAuthenticated()")
    public void enviarEmail(HttpServletResponse httpResponse) {
        var identidad = this.securityUtils.getIdentidadActual();
        var username = identidad.getUsername();
        this.showcaseService.enviarEmail(username);
        httpResponse.setStatus(HttpServletResponse.SC_OK);
    }

}
