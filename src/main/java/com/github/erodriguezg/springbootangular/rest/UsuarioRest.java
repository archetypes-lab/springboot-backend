package com.github.erodriguezg.springbootangular.rest;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.github.erodriguezg.springbootangular.dto.GuardarUsuarioDto;
import com.github.erodriguezg.springbootangular.dto.UsuarioFiltroDto;
import com.github.erodriguezg.springbootangular.entities.Usuario;
import com.github.erodriguezg.springbootangular.services.UsuarioService;
import com.github.erodriguezg.springbootangular.utils.HttpUtils;
import com.github.erodriguezg.springbootangular.utils.PropertyUtils;
import com.github.erodriguezg.springbootangular.utils.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/usuarios")
@PreAuthorize("isAuthenticated() and hasAnyAuthority('Administrador')")
public class UsuarioRest {

    private static final Logger log = LoggerFactory.getLogger(UsuarioRest.class);

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PropertyUtils propertyUtils;

    @Autowired
    private SecurityUtils securityUtils;

    @GetMapping("/id/{idUsuario}")
    public Usuario traerPorId(@PathVariable("idUsuario") Long idUsuario) {
        return usuarioService.traerPorId(idUsuario);
    }

    @GetMapping("/username/{username}")
    public Usuario traerPorUsername(@PathVariable("username") String username) {
        return usuarioService.traerPorUsername(username);
    }

    @PostMapping("/email")
    public Usuario traerPorEmail(@RequestParam("email") String email) {
        return usuarioService.traerPorEmail(email);
    }

    @GetMapping("/run/{run}")
    public Usuario traerPorRun(@PathVariable("run") Integer run) {
        return usuarioService.traerPorRun(run);
    }

    @PostMapping("/buscar/rowcount")
    public long buscarRowCount(@RequestBody UsuarioFiltroDto filtros) {
        log.debug("-> filtros entrada: {}", filtros);
        long rowCount = usuarioService.buscarRowCount(filtros);
        log.debug("-> rowCount: {}", rowCount);
        return rowCount;
    }

    @PostMapping("/buscar")
    public List<Usuario> buscar(@RequestBody UsuarioFiltroDto filtros, @RequestParam("inicio") int inicio,
            @RequestParam("fin") int fin) {
        log.debug("-> filtros entrada: {}", filtros);
        log.debug("-> inicio: {}", inicio);
        log.debug("-> fin: {}", fin);
        return usuarioService.buscar(filtros, inicio, fin);
    }

    @PostMapping("/buscar-no-paginado")
    public List<Usuario> buscarNoPaginado(@RequestBody UsuarioFiltroDto filtros) {
        log.debug("-> buscando usuarios por filtros: {}", filtros);
        return usuarioService.buscar(filtros);
    }

    @PostMapping("/guardar")
    public void guardar(@RequestBody @Valid GuardarUsuarioDto guardarUsuarioDto, BindingResult bindResult) {
        if (bindResult.hasErrors()) {
            log.warn("Error de entrada: {}", bindResult.getAllErrors());
            throw new IllegalArgumentException(bindResult.getAllErrors().toString());
        }

        // mapping para evitar vulnerabilidad de llenado entidad por post
        Usuario usuario = new Usuario();
        propertyUtils.copyProperties(usuario, guardarUsuarioDto);

        log.debug("Guardar usuario, entrada: {}", usuario);
        usuarioService.guardarUsuario(usuario);
    }

    @PostMapping("/eliminar")
    public void eliminar(@RequestParam("idUsuario") long idUsuario) {
        var identidad = securityUtils.getIdentidadActual();
        log.debug("-> principal: {}", identidad);
        Usuario usuario = new Usuario();
        usuario.setIdPersona(idUsuario);
        usuarioService.eliminar(usuario, -1L);
    }

    @PostMapping("/cambiar-pass")
    public void cambiarPass(@RequestParam("idUsuario") long idUsuario, @RequestParam("newPass") String newPass) {
        Usuario usuario = usuarioService.traerPorId(idUsuario);
        usuario.setPassword(newPass);
        usuarioService.guardarUsuario(usuario);
    }

    @GetMapping("/tiene-imagen-perfil/{idPersona}")
    public boolean tieneImagenPerfil(@PathVariable long idPersona) {
        return this.usuarioService.tieneImagenPerfil(idPersona);
    }

    @GetMapping("/descargar-imagen-perfil/{idPersona}")
    public void descargarImagenPerfil(@PathVariable long idPersona, HttpServletResponse httpResponse) {
        File imagenPerfil = this.usuarioService.traerImagenPerfil(idPersona);
        HttpUtils.downloadFile(httpResponse, imagenPerfil, true);
        httpResponse.setStatus(HttpServletResponse.SC_OK);
    }

    @PostMapping("/subir-imagen-perfil")
    public void uploadImagePerfil(@RequestParam("file") MultipartFile multipartFile,
            @RequestParam("idPersona") long idPersona, HttpServletResponse httpResponse) {
        try (var inputStream = multipartFile.getInputStream()) {
            this.usuarioService.guardarImagenPerfil(inputStream, multipartFile.getSize(), idPersona);
            httpResponse.setStatus(HttpServletResponse.SC_OK);
        } catch (final IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @GetMapping("/borrar-imagen-perfil/{idPersona}")
    public void borrarImagenPerfil(@PathVariable long idPersona, HttpServletResponse httpResponse) {
        this.usuarioService.borrarImagenPerfil(idPersona);
        httpResponse.setStatus(HttpServletResponse.SC_OK);
    }

}
