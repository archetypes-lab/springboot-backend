package com.github.erodriguezg.springbootangular.rest;

import java.util.Collections;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.github.erodriguezg.springbootangular.dto.CredencialesDto;
import com.github.erodriguezg.springbootangular.dto.RespuestaLoginDto;
import com.github.erodriguezg.springbootangular.dto.StringDto;
import com.github.erodriguezg.springbootangular.entities.Usuario;
import com.github.erodriguezg.springbootangular.security.Identidad;
import com.github.erodriguezg.springbootangular.services.UsuarioService;
import com.github.erodriguezg.springbootangular.utils.ConstantesUtil;
import com.github.erodriguezg.springbootangular.utils.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/security")
public class SecurityRest {

    private static final Logger log = LoggerFactory.getLogger(SecurityRest.class);

    private static final String ERROR_LOGIN = "Usuario o contraseña incorrecto";

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SecurityUtils securityUtils;

    // PUBLICOS

    @PostMapping("/login")
    @PreAuthorize("permitAll()")
    public RespuestaLoginDto login(@RequestBody @Valid CredencialesDto credenciales, BindingResult bindResult,
            HttpServletRequest httpRequest) {

        if (bindResult.hasErrors()) {
            RespuestaLoginDto respParamInvalidos = new RespuestaLoginDto();
            respParamInvalidos.setExitoLogin(false);
            respParamInvalidos.setErrores(Collections.singletonList(ConstantesUtil.MSJ_PARAMETROS_INVALIDOS));
            return respParamInvalidos;
        }

        // validar usuario

        Usuario usuarioEncontrado = this.usuarioService.traerPorUsername(credenciales.getUsername());
        if (usuarioEncontrado == null) {
            RespuestaLoginDto errorNoEncontroUsuario = new RespuestaLoginDto();
            errorNoEncontroUsuario.setErrores(Collections.singletonList("credenciales incorrectas"));
            errorNoEncontroUsuario.setExitoLogin(false);
            return errorNoEncontroUsuario;
        }

        // validar credenciales

        Optional<String> errorCredenciales = validarCredenciales(credenciales, usuarioEncontrado);
        if (errorCredenciales.isPresent()) {
            RespuestaLoginDto respuestaErrorCredenciales = new RespuestaLoginDto();
            respuestaErrorCredenciales.setExitoLogin(false);
            respuestaErrorCredenciales.setErrores(Collections.singletonList(errorCredenciales.get()));
            return respuestaErrorCredenciales;
        }

        // iniciar sesion usuario

        try {
            var identidad = new Identidad(usuarioEncontrado);

            // spring security context
            var auth = new UsernamePasswordAuthenticationToken(identidad, "", identidad.getAuthorities());
            var securityContex = SecurityContextHolder.getContext();
            securityContex.setAuthentication(auth);
            var session = httpRequest.getSession(true);
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContex);

            // response json
            RespuestaLoginDto respuesta = new RespuestaLoginDto();
            respuesta.setIdentidad(identidad);
            respuesta.setExitoLogin(true);
            return respuesta;
        } catch (RuntimeException ex) {
            log.error("Ocurrio un error al identificar usuario: {}", credenciales.getUsername(), ex);
            RespuestaLoginDto respuestaErrorInterno = new RespuestaLoginDto();
            respuestaErrorInterno.setExitoLogin(false);
            respuestaErrorInterno.setErrores(Collections.singletonList(ConstantesUtil.MSJ_ERROR_INTERNO));
            return respuestaErrorInterno;
        }

    }

    @GetMapping("/get-csrf-token")
    @PreAuthorize("permitAll()")
    public StringDto getCsrfToken(HttpServletRequest request) {
        var token = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
        return new StringDto(token.getToken());
    }

    @PostMapping("/get-identidad")
    @PreAuthorize("permitAll()")
    public Identidad getIdentidad() {
        return this.securityUtils.getIdentidadActual();
    }

    @GetMapping("/logout")
    @PreAuthorize("permitAll()")
    public void logout(HttpServletRequest httpRequest) {
        try {
            httpRequest.logout();
        }catch(ServletException ex) {
            log.error("Ocurrio un error al realizar logout", ex);
        }
    }

    // PRIVADOS

    private Optional<String> validarCredenciales(CredencialesDto credenciales, Usuario usuario) {
        if (usuario == null) {
            return Optional.of(ERROR_LOGIN);
        }
        if (!passwordEncoder.matches(credenciales.getPassword(), usuario.getPassword())) {
            return Optional.of(ERROR_LOGIN);
        }
        return Optional.empty();
    }

}
