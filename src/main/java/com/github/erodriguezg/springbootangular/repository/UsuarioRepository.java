package com.github.erodriguezg.springbootangular.repository;

import com.github.erodriguezg.springbootangular.entities.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Usuario findByUsername(String username);

    Usuario findByPersonaRun(Integer run);

    Usuario findByPersonaEmail(String email);

}
