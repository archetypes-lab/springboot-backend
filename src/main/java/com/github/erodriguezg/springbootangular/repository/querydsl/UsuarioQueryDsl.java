package com.github.erodriguezg.springbootangular.repository.querydsl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.github.erodriguezg.springbootangular.dto.UsuarioFiltroDto;
import com.github.erodriguezg.springbootangular.entities.QPerfilUsuario;
import com.github.erodriguezg.springbootangular.entities.QPersona;
import com.github.erodriguezg.springbootangular.entities.QUsuario;
import com.github.erodriguezg.springbootangular.entities.Usuario;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

import org.springframework.stereotype.Repository;

@Repository
public class UsuarioQueryDsl {

    @PersistenceContext
    private EntityManager em;

    public long buscarCount(UsuarioFiltroDto filtros) {
        var qUsuario = QUsuario.usuario;
        var qPersona = QPersona.persona;
        var qPerfil = QPerfilUsuario.perfilUsuario;
        return new JPAQuery<Usuario>(em)
            .select(qUsuario)
            .from(qUsuario)
            .distinct()
            .leftJoin(qUsuario.persona, qPersona)
            .leftJoin(qUsuario.perfil, qPerfil)
            .where(crearBuscarPredicate(filtros, qUsuario, qPersona, qPerfil))
            .fetchCount();
    }

    public List<Usuario> buscar(UsuarioFiltroDto filtros, int inicio, int fin) {
        int pageSize = fin - inicio;
    	int paginaActual = inicio / pageSize;
    	int offset = (paginaActual) * pageSize;

        var qUsuario = QUsuario.usuario;
        var qPersona = QPersona.persona;
        var qPerfil = QPerfilUsuario.perfilUsuario;
        return new JPAQuery<Usuario>(em)
            .select(qUsuario)
            .from(qUsuario)
            .distinct()
            .leftJoin(qUsuario.persona, qPersona).fetchJoin()
            .leftJoin(qUsuario.perfil, qPerfil).fetchJoin()
            .where(crearBuscarPredicate(filtros, qUsuario, qPersona, qPerfil))
            .orderBy(qPersona.nombres.asc())
    		.offset(offset)
    		.limit(pageSize)
    		.fetch();
    }

    public List<Usuario> buscar(UsuarioFiltroDto filtros) {
        return this.buscar(filtros, 0, Integer.MAX_VALUE);
    }

    private Predicate crearBuscarPredicate(UsuarioFiltroDto filtros, QUsuario qUsuario, QPersona qPersona,
            QPerfilUsuario qPerfil) {
        return ExpressionUtils.allOf(
            crearRunPredicate(filtros, qPersona), 
            crearNombresPredicate(filtros, qPersona),
            crearApPaternoPredicate(filtros, qPersona), 
            crearApMaternoPredicate(filtros, qPersona),
            crearEmailPredicate(filtros, qPersona), 
            crearFechaNacimientoInferiorPredicate(filtros, qPersona),
            crearFechaNacimientoSuperiorPredicate(filtros, qPersona), 
            crearHabilitadoPredicate(filtros, qUsuario),
            crearUsernamePredicate(filtros, qUsuario), 
            crearPerfilUsuarioPredicate(filtros, qPerfil));
    }

    private Predicate crearRunPredicate(UsuarioFiltroDto filtros, QPersona qPersona) {
        if (filtros.getRun() != null) {
            return qPersona.run.eq(filtros.getRun());
        }
        return null;
    }

    private Predicate crearNombresPredicate(UsuarioFiltroDto filtros, QPersona qPersona) {
        if (filtros.getNombres() != null) {
            return qPersona.nombres.likeIgnoreCase("%" + filtros.getApPaterno() + "%");
        }
        return null;
    }

    private Predicate crearApPaternoPredicate(UsuarioFiltroDto filtros, QPersona qPersona) {
        if (filtros.getApPaterno() != null) {
            return qPersona.apellidoPaterno.likeIgnoreCase("%" + filtros.getApPaterno() + "%");
        }
        return null;
    }

    private Predicate crearApMaternoPredicate(UsuarioFiltroDto filtros, QPersona qPersona) {
        if (filtros.getApMaterno() != null) {
            return qPersona.apellidoMaterno.likeIgnoreCase("%" + filtros.getApMaterno() + "%");
        }
        return null;
    }

    private Predicate crearEmailPredicate(UsuarioFiltroDto filtros, QPersona qPersona) {
        if (filtros.getEmail() != null) {
            return qPersona.email.likeIgnoreCase("%" + filtros.getEmail() + "%");
        }
        return null;
    }

    private Predicate crearFechaNacimientoInferiorPredicate(UsuarioFiltroDto filtros, QPersona qPersona) {
        if (filtros.getFechaNacimientoInferior() != null) {
            return qPersona.fechaNacimiento.after(filtros.getFechaNacimientoInferior());
        }
        return null;
    }

    private Predicate crearFechaNacimientoSuperiorPredicate(UsuarioFiltroDto filtros, QPersona qPersona) {
        if (filtros.getFechaNacimientoSuperior() != null) {
            return qPersona.fechaNacimiento.before(filtros.getFechaNacimientoSuperior());
        }
        return null;
    }

    private Predicate crearHabilitadoPredicate(UsuarioFiltroDto filtros, QUsuario qUsuario) {
        if (filtros.getHabilitado() != null) {
            return qUsuario.habilitado.eq(filtros.getHabilitado());
        }
        return null;
    }

    private Predicate crearUsernamePredicate(UsuarioFiltroDto filtros, QUsuario qUsuario) {
        if (filtros.getUsername() != null) {
            return qUsuario.username.likeIgnoreCase("%" + filtros.getUsername() + "%");
        }
        return null;
    }

    private Predicate crearPerfilUsuarioPredicate(UsuarioFiltroDto filtros, QPerfilUsuario qPerfil) {
        if (filtros.getPerfil() != null && filtros.getPerfil().getIdPerfilUsuario() != null) {
            return qPerfil.idPerfilUsuario.eq(filtros.getPerfil().getIdPerfilUsuario());
        }
        return null;
    }

}
