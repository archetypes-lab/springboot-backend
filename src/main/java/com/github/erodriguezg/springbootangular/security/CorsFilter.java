package com.github.erodriguezg.springbootangular.security;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CorsFilter implements Filter {


    private final List<String> allowOriginList;


    public CorsFilter(String allowOrigins) {
        if(allowOrigins == null || allowOrigins.isBlank()) {
            this.allowOriginList = List.of();
        }
        else {
            this.allowOriginList = List.of(allowOrigins.split(","))
                .stream()
                .map(String::trim)
                .collect(Collectors.toList());
        }
    }

    public CorsFilter(List<String> allowOriginList) {
        this.allowOriginList = allowOriginList;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        var httpResponse = (HttpServletResponse) res;
        var httpRequest = (HttpServletRequest) req;

        var originHeader = httpRequest.getHeader("origin");

        var existeHeader = verificarSiExisteOriginHeader(originHeader);

        httpResponse.setHeader("Access-Control-Allow-Origin", existeHeader ? originHeader : "");
        httpResponse.setHeader("Access-Control-Allow-Credentials", "true");
        httpResponse.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        httpResponse.setHeader("Access-Control-Max-Age", "3600");
        httpResponse.setHeader("Access-Control-Allow-Headers",
                "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-CSRF-TOKEN");
        chain.doFilter(req, res);
    }

    @Override
    public void init(FilterConfig filterConfig) {
        // empty method
    }

    @Override
    public void destroy() {
        // empty method
    }

    // privados

    private boolean verificarSiExisteOriginHeader(String originHeader) {
        return this.allowOriginList.contains("*") || this.allowOriginList.contains(originHeader);
    }

}
