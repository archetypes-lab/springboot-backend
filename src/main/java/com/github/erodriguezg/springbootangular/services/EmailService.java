package com.github.erodriguezg.springbootangular.services;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

import javax.mail.MessagingException;

import com.github.erodriguezg.springbootangular.utils.FileUtils;
import com.github.erodriguezg.springbootangular.utils.emails.EmailDto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    private static final Logger log = LoggerFactory.getLogger(EmailService.class);

    @Autowired
    private Queue emailQueue;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private JavaMailSender emailSender;

    @Value("${spring.mail.username}")
    private String emailFrom;

    public void sendEmail(EmailDto emailDto) {
        this.rabbitTemplate.convertAndSend(emailQueue.getName(), emailDto);
        log.debug("Email enviado '{}'", emailDto.getSubject());
    }

    @RabbitListener(queues = "#{emailQueue.name}")
    public void recibeEmail(EmailDto emailDto) {
        log.debug("Se recibio un nuevo email '{}''", emailDto.getSubject());
        var message = emailSender.createMimeMessage();
        MimeMessageHelper helper;
        List<File> tmpFiles = new ArrayList<>();
        try {

            boolean multipartMessage = haveAttachements(emailDto);

            helper = new MimeMessageHelper(message, multipartMessage, StandardCharsets.UTF_8.name());
            helper.setFrom(this.emailFrom);
            helper.setSubject(emailDto.getSubject());
            helper.setText(emailDto.getHtmlText(), true);
            helper.setTo(emailDto.getToList().toArray(new String[0]));
            if (emailDto.getCcList() != null && !emailDto.getCcList().isEmpty()) {
                helper.setCc(emailDto.getCcList().toArray(new String[0]));
            }
            if (emailDto.getBccList() != null && !emailDto.getBccList().isEmpty()) {
                helper.setBcc(emailDto.getBccList().toArray(new String[0]));
            }
            addInlines(helper, emailDto.getInlineAttachmentList(), tmpFiles);
            addAttachments(helper, emailDto.getAttachmentList(), tmpFiles);
            this.emailSender.send(message);
        } catch (MessagingException ex) {
            throw new IllegalStateException("Error al enviar e-mail. ", ex);
        } finally {
            cleanTmpFiles(tmpFiles);
        }
    }

    private static boolean haveAttachements(EmailDto emailDto) {
        if (emailDto == null) {
            return false;
        }
        boolean haveAttachments = emailDto.getAttachmentList() != null && !emailDto.getAttachmentList().isEmpty();
        boolean haveInline = emailDto.getInlineAttachmentList() != null
                && !emailDto.getInlineAttachmentList().isEmpty();
        return haveAttachments || haveInline;
    }

    private static void addInlines(final MimeMessageHelper helper, final List<EmailDto.AttachmentDto> attachments,
            final List<File> tmpFiles) {
        addAttachments(attachments, tmpFiles, (attachmentTmpFile, attachmentName) -> {
            try {
                helper.addInline(attachmentName, attachmentTmpFile);
            } catch (MessagingException e) {
                throw new IllegalStateException(e);
            }
        });
    }

    private static void addAttachments(final MimeMessageHelper helper, final List<EmailDto.AttachmentDto> attachments,
            final List<File> tmpFiles) {
        addAttachments(attachments, tmpFiles, (attachmentTmpFile, attachmentName) -> {
            try {
                helper.addAttachment(attachmentName, attachmentTmpFile);
            } catch (MessagingException e) {
                throw new IllegalStateException(e);
            }
        });
    }

    private static void addAttachments(final List<EmailDto.AttachmentDto> attachments, final List<File> tmpFiles,
            final BiConsumer<File, String> biConsumer) {
        if (attachments == null) {
            return;
        }
        for (var attachment : attachments) {
            var attachmentName = attachment.getName();
            var attachmentTmpFile = FileUtils.copyTmpBase64Url(attachment.getB64UrlData(), attachmentName);
            tmpFiles.add(attachmentTmpFile);
            biConsumer.accept(attachmentTmpFile, attachmentName);
        }
    }

    private static void cleanTmpFiles(List<File> tmpFiles) {
        if (tmpFiles == null) {
            return;
        }
        for (var tmpFile : tmpFiles) {
            FileUtils.deleteQuietly(tmpFile);
        }
    }

}