package com.github.erodriguezg.springbootangular.services;

import java.io.File;
import java.util.Optional;

import com.github.erodriguezg.springbootangular.email.HolaMundoEmail;
import com.github.erodriguezg.springbootangular.pdf.HolaMundoPdf;
import com.github.erodriguezg.springbootangular.utils.storage.StorageUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ShowcaseService {

    private static final String DIRECTORIO_ARCHIVO = "showcase";
    private static final String ARCHIVO = "hola_mundo.pdf";

    @Value("${spring.mail.username}")
    private String emailPropio;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private StorageUtils storageUtils;

    @Autowired
    private HolaMundoPdf holaMundoPdf;

    @Autowired
    private HolaMundoEmail holaMundoEmail;

    /* ARCHIVOS Y PDF */

    @Transactional(readOnly = true)
    public void generarYSubirAchivo(String username) {
        var usuario = this.usuarioService.traerPorUsername(username);
        var bytesPdf = this.holaMundoPdf.generatePdf(usuario);
        this.storageUtils.put(getPathArchivo(), bytesPdf);
    }

    public void eliminarArchivo() {
        this.storageUtils.delete(getPathArchivo());
    }

    public Optional<File> obtenerArchivo() {
        return this.storageUtils.getTemporalFile(getPathArchivo());
    }

    /* COLAS Y EMAILS */

    public void enviarEmail(String username) {
        var usuario = this.usuarioService.traerPorUsername(username);

        var bytesPdf = this.holaMundoPdf.generatePdf(usuario);

        var emailDto = this.holaMundoEmail.generateEmail(usuario, this.emailPropio, bytesPdf);

        this.emailService.sendEmail(emailDto);
    }

    // private

    private static String getPathArchivo() {
        return String.format("%s/%s", DIRECTORIO_ARCHIVO, ARCHIVO);
    }

}