package com.github.erodriguezg.springbootangular.services;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.github.erodriguezg.springbootangular.dto.UsuarioFiltroDto;
import com.github.erodriguezg.springbootangular.entities.Persona;
import com.github.erodriguezg.springbootangular.entities.Usuario;
import com.github.erodriguezg.springbootangular.repository.UsuarioRepository;
import com.github.erodriguezg.springbootangular.repository.querydsl.UsuarioQueryDsl;
import com.github.erodriguezg.springbootangular.services.exceptions.LogicaNegocioException;
import com.github.erodriguezg.springbootangular.utils.ConstantesUtil;
import com.github.erodriguezg.springbootangular.utils.storage.StorageUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class UsuarioService {

    private static final Logger LOG = LoggerFactory.getLogger(UsuarioService.class);

    private static final String RUTA_PLANTILLA_IMAGENES_PERFIL = "usuarios/imagenes-perfil/{idUsuario}.png";

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioQueryDsl usuarioQueryDsl;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private StorageUtils storageUtils;

    public List<Usuario> traerTodos() {
        return usuarioRepository.findAll(Sort.by(Sort.Direction.ASC, "usuario"));
    }

    public Usuario traerPorEmail(final String email) {
        return usuarioRepository.findByPersonaEmail(email != null ? email.trim().toLowerCase() : null);
    }

    @Transactional(readOnly = false)
    public Usuario guardarUsuario(Usuario usuarioParam) {
        LOG.debug("Guardar Usuario: {}", usuarioParam);

        Persona personaParam = usuarioParam.getPersona();

        Usuario usuarioAux = usuarioRepository.findByPersonaEmail(personaParam.getEmail());
        if (usuarioAux != null && !usuarioAux.getIdPersona().equals(usuarioParam.getIdPersona())) {
            throw new LogicaNegocioException("Ya existe correo para el usuario");
        }

        usuarioAux = usuarioRepository.findByPersonaRun(personaParam.getRun());
        if (usuarioAux != null && !usuarioAux.getIdPersona().equals(usuarioParam.getIdPersona())) {
            throw new LogicaNegocioException("Ya existe run para el usuario");
        } else if (usuarioAux == null) {
            usuarioParam.setHabilitado(true);
        }

        if (usuarioAux == null || !usuarioAux.getPassword().equals(usuarioParam.getPassword())) {
            usuarioParam.setPassword(passwordEncoder.encode(usuarioParam.getPassword()));
        }
        if (usuarioAux != null) {
            usuarioParam.getPersona().setRutaArchivoImagenPerfil(usuarioAux.getPersona().getRutaArchivoImagenPerfil());
        }
        Usuario usuario = usuarioParam;
        Persona persona = em.merge(usuario.getPersona());
        persona.setUsuario(usuario);
        usuario.setPersona(persona);
        if (usuarioAux == null) {
            em.persist(usuario);
        } else {
            usuario.setHabilitado(usuarioAux.getHabilitado());
            usuario = em.merge(usuario);
        }
        return usuario;
    }

    public Usuario traerPorUsername(final String username) {
        return usuarioRepository.findByUsername(username != null ? username.trim().toLowerCase() : null);
    }

    public long buscarRowCount(UsuarioFiltroDto filtros) {
        return this.usuarioQueryDsl.buscarCount(filtros);
    }

    public List<Usuario> buscar(UsuarioFiltroDto filtros, int inicio, int fin) {
        return this.usuarioQueryDsl.buscar(filtros, inicio, fin);
    }

    public List<Usuario> buscar(UsuarioFiltroDto usuarioFiltroDto) {
        return buscar(usuarioFiltroDto, 0, Integer.MAX_VALUE);
    }

    @Transactional(readOnly = false)
    public void eliminar(Usuario usuarioParam, Long idUsuarioActual) {
        if (idUsuarioActual.equals(usuarioParam.getIdPersona())) {
            throw new LogicaNegocioException(ConstantesUtil.MSJ_ELIMINAR_A_SI_MISMO);
        }
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(usuarioParam.getIdPersona());
        if (optionalUsuario.isPresent()) {
            Usuario usuario = optionalUsuario.get();
            Persona persona = usuario.getPersona();
            em.remove(usuario);
            em.remove(persona);
        }
    }

    @Transactional(readOnly = false)
    public void habilitar(Usuario usuarioParam) {
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(usuarioParam.getIdPersona());
        if (optionalUsuario.isPresent()) {
            Usuario usuario = optionalUsuario.get();
            usuario.setHabilitado(true);
        }
    }

    @Transactional(readOnly = false)
    public void deshabilitar(Usuario usuarioPAram, Long idUsuarioActual) {
        if (idUsuarioActual.equals(usuarioPAram.getIdPersona())) {
            throw new LogicaNegocioException(ConstantesUtil.MSJ_DESHABILITAR_A_SI_MISMO);
        }
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(usuarioPAram.getIdPersona());
        if (optionalUsuario.isPresent()) {
            Usuario usuario = optionalUsuario.get();
            usuario.setHabilitado(false);
        }
    }

    public Usuario traerPorRun(Integer run) {
        return usuarioRepository.findByPersonaRun(run);
    }

    public Usuario traerPorId(Long idUsuario) {
        return usuarioRepository.findById(idUsuario).orElse(null);
    }

    public boolean tieneImagenPerfil(Long idPersona) {
        var usuario = getUsuario(idPersona);
        var rutaImagenPerfil = usuario.getPersona().getRutaArchivoImagenPerfil();
        return rutaImagenPerfil != null;
    }

    public File traerImagenPerfil(Long idPersona) {
        var usuario = getUsuario(idPersona);
        var rutaImagenPerfil = usuario.getPersona().getRutaArchivoImagenPerfil();
        if (rutaImagenPerfil == null) {
            throw new IllegalStateException(
                    String.format("No existe imagen de perfil para el usuario id: %d", idPersona));
        }
        var optionalFile = this.storageUtils.getTemporalFile(rutaImagenPerfil);
        if (!optionalFile.isPresent()) {
            throw new IllegalStateException(
                    String.format("No existe el recurso '%s' en el storage.", rutaImagenPerfil));
        }
        return optionalFile.get();
    }

    @Transactional(readOnly = false)
    public void guardarImagenPerfil(InputStream inputStream, long bytesSize, long idPersona) {
        var usuario = getUsuario(idPersona);
        var rutaImagenPerfil = RUTA_PLANTILLA_IMAGENES_PERFIL.replace("{idUsuario}", String.format("%010d", idPersona));
        usuario.getPersona().setRutaArchivoImagenPerfil(rutaImagenPerfil);
        this.storageUtils.put(rutaImagenPerfil, inputStream, bytesSize);
    }

    @Transactional(readOnly = false)
    public void borrarImagenPerfil(long idPersona) {
        var usuario = getUsuario(idPersona);
        var rutaImagenPerfil = usuario.getPersona().getRutaArchivoImagenPerfil();
        usuario.getPersona().setRutaArchivoImagenPerfil(null);
        this.storageUtils.delete(rutaImagenPerfil);
    }

    // private

    private Usuario getUsuario(Long idPersona) {
        var opUsuario = this.usuarioRepository.findById(idPersona);
        if (!opUsuario.isPresent()) {
            throw new IllegalStateException(String.format("No se encontre el usuario con id: %d", idPersona));
        }
        return opUsuario.get();
    }

}
