package com.github.erodriguezg.springbootangular.pdf;

import java.util.Map;

import com.github.erodriguezg.springbootangular.utils.FileUtils;
import com.github.erodriguezg.springbootangular.utils.pdfutils.PdfGenerator;
import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfFileResource;
import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfResource;
import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfResourceType;
import com.github.erodriguezg.springbootangular.utils.templates.TemplateProcessor;

import org.apache.commons.lang3.ArrayUtils;

public abstract class PlantillaPdf extends AbstractPdf {

    public PlantillaPdf(TemplateProcessor templateProcessor, PdfGenerator pdfGenerator) {
        super(templateProcessor, pdfGenerator);
    }

    @Override
    protected byte[] generatePdf(Map<String, Object> model, PdfResource[] resources) {
        
        if(resources == null) {
            resources = new PdfResource[]{};
        }

        // Fuente
        var helveticaFontResource = new PdfFileResource("helvetica", PdfResourceType.FONT, 
        FileUtils.copyTmpClasspathFile("/fonts/helvetica/Helvetica.ttf"));

        // Logo Cabecera
        var logoHeadedrImgResource = new PdfFileResource("logo-header", PdfResourceType.IMG, 
        FileUtils.copyTmpClasspathFile("/imgs/logo_cabecera_reporte.png"));

        return super.generatePdf(model, ArrayUtils.addAll(resources, helveticaFontResource, logoHeadedrImgResource));
    }
    
}