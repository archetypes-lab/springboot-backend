package com.github.erodriguezg.springbootangular.pdf;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.github.erodriguezg.springbootangular.entities.Usuario;
import com.github.erodriguezg.springbootangular.utils.FileUtils;
import com.github.erodriguezg.springbootangular.utils.pdfutils.PdfGenerator;
import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfFileResource;
import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfResource;
import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfResourceType;
import com.github.erodriguezg.springbootangular.utils.templates.TemplateProcessor;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HolaMundoPdf extends PlantillaPdf {

    @Autowired
    public HolaMundoPdf(TemplateProcessor templateProcessor, PdfGenerator pdfGenerator) {
        super(templateProcessor, pdfGenerator);
    }

    @Override
    protected String getTemplate() {
        return "/freemarker-templates/pdfs/hola_mundo.ftl";
    }

    public byte[] generatePdf(Usuario usuario) {

        File grafico1TmpFile = FileUtils.createTempFile(".png");
        try {

            // cargar modelo
            Map<String, Object> model = new HashMap<>();
            model.put("usuario", usuario);

            // generar grafico
            var chart = createChart(createDataset());
            ChartUtils.saveChartAsPNG(grafico1TmpFile, chart, 800, 600);

            // recursos

            // generado al vuelo
            var grafico1Resource = new PdfFileResource("grafico1", PdfResourceType.IMG, grafico1TmpFile);

            // generar

            return this.generatePdf(model, new PdfResource[] { grafico1Resource });

        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        } finally {
            FileUtils.deleteQuietly(grafico1TmpFile);
        }

    }

    private static PieDataset createDataset() {
        var dataset = new DefaultPieDataset();
        dataset.setValue("IPhone 5s", 20D);
        dataset.setValue("SamSung Grand", 20D);
        dataset.setValue("MotoG", 40D);
        dataset.setValue("Nokia Lumia", 10D);
        return dataset;
    }

    private static JFreeChart createChart(PieDataset dataset) {
        return ChartFactory.createPieChart("Mobile Sales", // chart title
                dataset, // data
                true, // include legend
                true, false);
    }

}