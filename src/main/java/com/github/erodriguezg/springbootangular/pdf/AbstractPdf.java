package com.github.erodriguezg.springbootangular.pdf;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import com.github.erodriguezg.springbootangular.utils.FileUtils;
import com.github.erodriguezg.springbootangular.utils.pdfutils.PdfGenerator;
import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfFileResource;
import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfResource;
import com.github.erodriguezg.springbootangular.utils.templates.TemplateProcessor;

public abstract class AbstractPdf {

    private final TemplateProcessor templateProcessor;

    private final PdfGenerator pdfGenerator;

    public AbstractPdf(TemplateProcessor templateProcessor, PdfGenerator pdfGenerator) {
        this.templateProcessor = templateProcessor;
        this.pdfGenerator = pdfGenerator;
    }

    protected abstract String getTemplate();

    protected byte[] generatePdf(Map<String, Object> model, PdfResource[] resources) {
        try {
            var template = this.getTemplate();
            var templateEvaluated = this.templateProcessor.process(template, model);
            return this.pdfGenerator.generatePdf(templateEvaluated.getBytes(StandardCharsets.UTF_8), resources);
        } finally {
            cleanResources(resources);
        }
    }

    private static void cleanResources(PdfResource[] resources) {
        if (resources == null) {
            return;
        }
        for (var resource : resources) {
            if (!(resource instanceof PdfFileResource)) {
                continue;
            }
            var file = ((PdfFileResource) resource).getFile();
            FileUtils.deleteQuietly(file);
        }
    }

}