package com.github.erodriguezg.springbootangular.dto;

import java.util.Date;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class InfoDto {

    private Date fechaPresente;

    private Integer sesionTimeoutSegundos;

}