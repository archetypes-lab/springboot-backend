package com.github.erodriguezg.springbootangular.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.github.erodriguezg.springbootangular.entities.PerfilUsuario;
import com.github.erodriguezg.springbootangular.entities.Persona;

import lombok.Data;

@Data
public class GuardarUsuarioDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPersona;

    @NotBlank
    @Size(min = 4, max = 20)
    private String username;

    private String password;

    @NotNull
    private PerfilUsuario perfil;

    @NotNull
    private Persona persona;

}


