package com.github.erodriguezg.springbootangular.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

import com.github.erodriguezg.springbootangular.security.Identidad;

@Data
public class RespuestaLoginDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Identidad identidad;
    
    private Boolean exitoLogin;

    private List<String> errores;

}
