package com.github.erodriguezg.springbootangular.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RefreshTokenDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String token;

}
