package com.github.erodriguezg.springbootangular.dto;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

@Data
public class ErrorResponseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String errorType;

    private String message;

    private Map<String, Serializable> values;

}
