package com.github.erodriguezg.springbootangular.email;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.erodriguezg.springbootangular.entities.Usuario;
import com.github.erodriguezg.springbootangular.utils.emails.EmailDto;
import com.github.erodriguezg.springbootangular.utils.templates.TemplateProcessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HolaMundoEmail extends PlantillaEmail {

    @Autowired
    public HolaMundoEmail(TemplateProcessor templateProcessor) {
        super(templateProcessor);
    }

    @Override
    protected String getTemplate() {
        return "/freemarker-templates/emails/hola_mundo.ftl";
    }

    public EmailDto generateEmail(Usuario usuario, String fromTo, byte[] pdfBytes) {

        // cargar modelo
        Map<String, Object> model = new HashMap<>();
        model.put("usuario", usuario);

        // adjunto
        var attachment = createAttachment(pdfBytes, "holamundo.pdf");

        var inline1 = createAttachment(HolaMundoEmail.class.getResourceAsStream("/imgs/email-image1.jpg"),
                "image1.jpg");

        return generateEmail("Hola Mundo Email!", fromTo, List.of(fromTo), null, model, List.of(attachment),
                List.of(inline1));
    }

}