package com.github.erodriguezg.springbootangular.email;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.github.erodriguezg.springbootangular.utils.FileUtils;
import com.github.erodriguezg.springbootangular.utils.emails.EmailDto;
import com.github.erodriguezg.springbootangular.utils.templates.TemplateProcessor;

import org.apache.commons.io.IOUtils;

public abstract class AbstractEmail {

    private final TemplateProcessor templateProcessor;

    public AbstractEmail(TemplateProcessor templateProcessor) {
        this.templateProcessor = templateProcessor;
    }

    protected abstract String getTemplate();

    protected EmailDto generateEmail(String subject, String from, List<String> toList, List<String> ccList,
            Map<String, Object> model, List<EmailDto.AttachmentDto> attachments, List<EmailDto.AttachmentDto> inlines) {

        if (subject == null || subject.isBlank()) {
            throw new IllegalArgumentException("subject argument can't be null or blank");
        }

        if (from == null || from.isBlank()) {
            throw new IllegalArgumentException("from argument can't be null or blank");
        }

        if (toList == null || toList.isEmpty()) {
            throw new IllegalArgumentException("toList argument can't be null");
        }

        var template = this.getTemplate();
        var templateEvaluated = this.templateProcessor.process(template, model);

        var email = new EmailDto();
        email.setSubject(subject);
        email.setFrom(from);
        email.setToList(toList);
        email.setCcList(ccList != null ? ccList : Collections.emptyList());
        email.setHtmlText(templateEvaluated);
        email.setAttachmentList(attachments != null ? attachments : Collections.emptyList());
        email.setInlineAttachmentList(inlines != null ? inlines : Collections.emptyList());

        return email;
    }

    protected EmailDto.AttachmentDto createAttachment(File file) {
        var name = FileUtils.prettyFileName(file);
        return createAttachment(file, name);
    }

    protected EmailDto.AttachmentDto createAttachment(File file, String name) {
        try {
            return createAttachment(new FileInputStream(file), name);
        } catch (FileNotFoundException ex) {
            throw new IllegalStateException(ex);
        }
    }

    protected EmailDto.AttachmentDto createAttachment(InputStream inputStream, String name) {
        try (inputStream) {
            try (var outputStream = new ByteArrayOutputStream()) {
                IOUtils.copy(inputStream, outputStream);
                return createAttachment(outputStream.toByteArray(), name);
            }
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    protected EmailDto.AttachmentDto createAttachment(byte[] bytes, String name) {
        var attachment = new EmailDto.AttachmentDto();
        attachment.setName(name);
        var b64UrlData = Base64.getUrlEncoder().encodeToString(bytes);
        attachment.setB64UrlData(b64UrlData);
        return attachment;
    }

}