package com.github.erodriguezg.springbootangular.email;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.github.erodriguezg.springbootangular.utils.emails.EmailDto;
import com.github.erodriguezg.springbootangular.utils.emails.EmailDto.AttachmentDto;
import com.github.erodriguezg.springbootangular.utils.templates.TemplateProcessor;

public abstract class PlantillaEmail extends AbstractEmail {

    public PlantillaEmail(TemplateProcessor templateProcessor) {
        super(templateProcessor);
    }

    @Override
    protected EmailDto generateEmail(String subject, String from, List<String> toList, List<String> ccList,
            Map<String, Object> model, List<AttachmentDto> attachments, List<AttachmentDto> inlines) {

        var newInlines = new ArrayList<>(inlines != null ? inlines : Collections.emptyList());

        var inlineLogo = createAttachment(PlantillaEmail.class.getResourceAsStream("/imgs/email-logo.png"), "logo.png");
        var inlineFooter = createAttachment(PlantillaEmail.class.getResourceAsStream("/imgs/email-footer.png"),
                "footer.png");

        newInlines.add(inlineLogo);
        newInlines.add(inlineFooter);

        return super.generateEmail(subject, from, toList, ccList, model, attachments, newInlines);
    }

}