package com.github.erodriguezg.springbootangular.utils.emails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class EmailDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String from;

    private List<String> toList = new ArrayList<>();

    private List<String> ccList = new ArrayList<>();

    private List<String> bccList = new ArrayList<>();

    private String subject;

    private String htmlText;

    private List<AttachmentDto> inlineAttachmentList = new ArrayList<>();

    private List<AttachmentDto> attachmentList = new ArrayList<>();

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public static class AttachmentDto implements Serializable {

        private static final long serialVersionUID = 1L;

        private String name;

        private String b64UrlData;

    }

}