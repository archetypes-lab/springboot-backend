package com.github.erodriguezg.springbootangular.utils.pdfutils.resources;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.IOUtils;

public class PdfResourceUtil {

    private PdfResourceUtil() {
        // privado
    }

    static PdfResource[] getResourcesByType(PdfResource[] resources, PdfResourceType inputType) {

        List<PdfResource> resourceList = new ArrayList<>();

        if (resources == null || resources.length == 0) {
            return resourceList.toArray(new PdfResource[0]);
        }

        for (PdfResource resource : resources) {
            if (resource.getResourceType() == inputType) {
                resourceList.add(resource);
            }
        }

        return resourceList.toArray(new PdfResource[0]);
    }

    static byte[] getResourceBytes(PdfResource resource) {

        if (resource instanceof PdfFileResource) {
            File fileResource = ((PdfFileResource) resource).getFile();
            try (InputStream is = new FileInputStream(fileResource)) {
                return IOUtils.toByteArray(is);
            } catch (IOException ex) {
                throw new IllegalStateException("error obtener bytes! " + resource.getName(), ex);
            }
        } else if (resource instanceof PdfRawResource) {
            return ((PdfRawResource) resource).getRaw();
        }

        throw new IllegalStateException("Illegal Resource Format!");
    }

    static File crearFileTmp(PdfRawResource rawResource) {
        File tmpFile;
        try {
            tmpFile = File.createTempFile(UUID.randomUUID().toString(),
                    rawResource.getName() + "." + rawResource.getResourceType().name());
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
        try (OutputStream os = new FileOutputStream(tmpFile);
                InputStream is = new ByteArrayInputStream(rawResource.getRaw())) {
            IOUtils.copy(is, os);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
        return tmpFile;
    }

}