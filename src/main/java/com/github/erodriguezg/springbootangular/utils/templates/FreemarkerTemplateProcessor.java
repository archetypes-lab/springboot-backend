package com.github.erodriguezg.springbootangular.utils.templates;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreemarkerTemplateProcessor implements TemplateProcessor {

    public static final Logger log = LoggerFactory.getLogger(FreemarkerTemplateProcessor.class);

    private final Configuration freemarkerConfig;

    public FreemarkerTemplateProcessor() {
        this(createInstanceConfigurer());
    }

    private static Configuration createInstanceConfigurer() {
        return new Configuration(Configuration.VERSION_2_3_30);
    }

    public FreemarkerTemplateProcessor(Configuration freemarkerConfig) {
        log.info("==> INICIANDO PROCESADOR DE PLANTILLAS FREEMARKER");
        this.freemarkerConfig = freemarkerConfig;
        this.freemarkerConfig.setDefaultEncoding(StandardCharsets.UTF_8.name());
        var classLoader = this.getClass().getClassLoader();
        this.freemarkerConfig.setClassLoaderForTemplateLoading(classLoader, "/");
    }

    @Override
    public String process(String template, Object model) {
        Template t;
        try {
            t = this.freemarkerConfig.getTemplate(template);
        } catch (IOException e) {
            throw new IllegalStateException("Ocurrio un error obteniendo la template: " + template);
        }
        try {
            return FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
        } catch (IOException | TemplateException e) {
            throw new IllegalStateException("Ocurrio un error procesando la template: " + template);
        }
    }

}