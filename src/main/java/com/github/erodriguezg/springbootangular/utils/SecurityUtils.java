package com.github.erodriguezg.springbootangular.utils;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.github.erodriguezg.springbootangular.security.Identidad;

public class SecurityUtils {

    public Identidad getIdentidadActual() {
        var auth = SecurityContextHolder.getContext().getAuthentication();
        if(!isAnonymousUser(auth)) {
            return (Identidad) auth.getPrincipal();
        }
        return null;
    }

    private static boolean isAnonymousUser(Authentication auth) {
        return auth instanceof AnonymousAuthenticationToken;
    } 

}
