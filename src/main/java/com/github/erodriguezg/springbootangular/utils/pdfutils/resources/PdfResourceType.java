package com.github.erodriguezg.springbootangular.utils.pdfutils.resources;

public enum PdfResourceType {
    CSS, IMG, FONT, TEMPLATE
}