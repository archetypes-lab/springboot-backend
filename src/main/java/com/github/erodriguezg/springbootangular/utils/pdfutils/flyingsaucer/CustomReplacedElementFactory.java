package com.github.erodriguezg.springbootangular.utils.pdfutils.flyingsaucer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfFileResource;
import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfResource;
import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfResourceType;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Image;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Element;
import org.xhtmlrenderer.extend.FSImage;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextImageElement;
import org.xhtmlrenderer.render.BlockBox;
import org.xhtmlrenderer.simple.extend.FormSubmissionListener;

public class CustomReplacedElementFactory implements ReplacedElementFactory {

    private final ReplacedElementFactory superFactory;

    private final Map<String, PdfFileResource> imgFiles;

    public CustomReplacedElementFactory(ReplacedElementFactory superFactory, PdfResource[] resources) {
        this.superFactory = superFactory;
        imgFiles = filtrarImagenes(resources);
    }

    private Map<String, PdfFileResource> filtrarImagenes(PdfResource[] resources) {
        if (resources == null || resources.length == 0) {
            return Collections.emptyMap();
        }
        return Arrays.asList(resources).stream().filter(r -> r.getResourceType() == PdfResourceType.IMG)
                .collect(Collectors.toMap(PdfResource::getName, r -> (PdfFileResource) r));
    }

    @Override
    public ReplacedElement createReplacedElement(LayoutContext layoutContext, BlockBox blockBox,
            UserAgentCallback userAgentCallback, int cssWidth, int cssHeight) {

        Element element = blockBox.getElement();
        if (element == null) {
            return null;
        }

        String nodeName = element.getNodeName();
        String srcName = element.getAttribute("src");

        if (!"img".equalsIgnoreCase(nodeName)
                && (srcName == null || srcName.trim().isEmpty() || !imgFiles.containsKey(srcName.trim()))) {
            return superFactory.createReplacedElement(layoutContext, blockBox, userAgentCallback, cssWidth, cssHeight);
        }

        PdfFileResource fileResource = imgFiles.get(srcName.trim());
        try (InputStream inputStream = new FileInputStream(fileResource.getFile())) {
            byte[] bytes = IOUtils.toByteArray(inputStream);
            Image image = Image.getInstance(bytes);
            FSImage fsImage = new ITextFSImage(image);
            if ((cssWidth != -1) || (cssHeight != -1)) {
                fsImage.scale(cssWidth, cssHeight);
            }
            return new ITextImageElement(fsImage);
        } catch (IOException | BadElementException ex) {
            throw new IllegalStateException("error al escribir imagen en pdfutils. ", ex);
        }
    }

    @Override
    public void reset() {
        superFactory.reset();
    }

    @Override
    public void remove(Element e) {
        superFactory.remove(e);
    }

    @Override
    public void setFormSubmissionListener(FormSubmissionListener listener) {
        superFactory.setFormSubmissionListener(listener);
    }

}