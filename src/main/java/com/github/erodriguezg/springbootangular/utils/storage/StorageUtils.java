package com.github.erodriguezg.springbootangular.utils.storage;

import java.io.File;
import java.io.InputStream;
import java.util.Optional;

public interface StorageUtils {

    // put

    void put(String path, byte[] bytes);

    void put(String path, InputStream inputStream, long bytesSize);

    void put(String path, File file);

    // delete

    void delete(String path);

    // get

    Optional<byte[]> getByteArray(String path);

    Optional<InputStream> getInputStream(String path);

    Optional<File> getTemporalFile(String path);

}