package com.github.erodriguezg.springbootangular.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.tika.Tika;

public final class FileUtils {

    private FileUtils() {
        // no se puede instanciar
    }

    public static void deleteQuietly(File file) {
        org.apache.commons.io.FileUtils.deleteQuietly(file);
    }

    public static String getContentType(File file) {
        try {
            return new Tika().detect(file);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static String prettyFileName(File file) {
        var name = file.getName();
        return name.replaceAll("\\s+", "_");
    }

    public static File createTempFile(String suffix) {
        return createTempFile(UUID.randomUUID().toString(), suffix);
    }

    public static File createTempFile(String prefix, String suffix) {
        try {
            return Files.createTempFile(prefix, suffix).toFile();
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static File copyTmpClasspathFile(String path) {
        var extension = getExtension(path);
        var tmpFile = createTempFile(extension);

        try (var inputStream = FileUtils.class.getResourceAsStream(path)) {
            try (var outputStream = new FileOutputStream(tmpFile)) {
                IOUtils.copy(inputStream, outputStream);
                return tmpFile;
            }
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static String getExtension(String path) {
        var pathArray = path.split("\\.");
        if (pathArray.length <= 1) {
            return ".tmp";
        }
        return "." + pathArray[pathArray.length - 1];
    }

    public static File copyTmpBase64Url(String base64UrlData, String fileName) {
        var tmpFile = createTempFile("_" + fileName);

        byte[] bytes = Base64.getUrlDecoder().decode(base64UrlData);

        try (var inputStream = new ByteArrayInputStream(bytes)) {
            try (var outputSteam = new FileOutputStream(tmpFile)) {
                IOUtils.copy(inputStream, outputSteam);
                return tmpFile;
            }
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

}