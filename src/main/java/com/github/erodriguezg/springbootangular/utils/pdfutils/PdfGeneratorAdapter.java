package com.github.erodriguezg.springbootangular.utils.pdfutils;

import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfResource;

public abstract class PdfGeneratorAdapter implements PdfGenerator {

    @Override
    public byte[] generatePdf(byte[] xhtmlByte, PdfResource[] resources) {
        return generatePdf(xhtmlByte, resources, null);
    }

}
