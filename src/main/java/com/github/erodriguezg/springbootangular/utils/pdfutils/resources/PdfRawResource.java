package com.github.erodriguezg.springbootangular.utils.pdfutils.resources;

public class PdfRawResource extends PdfResource {

    private byte[] raw;

    public PdfRawResource(String name, PdfResourceType resourceType, byte[] raw) {
        this.raw = raw;
        this.setName(name);
        this.setResourceType(resourceType);
    }

    public byte[] getRaw() {
        return raw;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

}