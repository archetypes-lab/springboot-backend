package com.github.erodriguezg.springbootangular.utils.pdfutils.resources;

import java.io.File;

public class PdfFileResource extends PdfResource {
    private final File file;

    public PdfFileResource(String name, PdfResourceType resourceType, File file) {
        this.file = file;
        this.setName(name);
        this.setResourceType(resourceType);
    }

    public File getFile() {
        return file;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
    
}
