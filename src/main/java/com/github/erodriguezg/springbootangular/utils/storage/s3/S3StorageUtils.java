package com.github.erodriguezg.springbootangular.utils.storage.s3;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Optional;
import java.util.function.Function;

import com.github.erodriguezg.springbootangular.utils.FileUtils;
import com.github.erodriguezg.springbootangular.utils.storage.StorageUtils;

import org.apache.commons.io.IOUtils;
import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.blobstore.domain.Blob;

public class S3StorageUtils implements StorageUtils {

    private final ContextBuilder contextBuilder;
    private final String bucket;

    public S3StorageUtils(final ContextBuilder contextBuilder, final String bucket) {
        this.contextBuilder = contextBuilder;
        this.bucket = bucket;
    }

    @Override
    public void put(final String path, final byte[] bytes) {
        put(this.contextBuilder, this.bucket,
                blobStore -> blobStore.blobBuilder(path).payload(bytes).contentLength(bytes.length).build());
    }

    @Override
    public void put(final String path, final InputStream inputStream, final long bytesSize) {
        try (inputStream) {
            put(this.contextBuilder, this.bucket,
                    blobStore -> blobStore.blobBuilder(path).payload(inputStream).contentLength(bytesSize).build());
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public void put(final String path, final File file) {
        put(this.contextBuilder, this.bucket,
                blobStore -> blobStore.blobBuilder(path).payload(file).contentLength(file.length()).build());
    }

    @Override
    public void delete(final String path) {
        try (var context = contextBuilder.buildView(BlobStoreContext.class)) {
            var blobStore = context.getBlobStore();
            blobStore.removeBlob(this.bucket, path);
        }
    }

    @Override
    public Optional<byte[]> getByteArray(final String path) {
        var optionalStream = getInputStream(path);
        if (!optionalStream.isPresent()) {
            return Optional.<byte[]>empty();
        }
        try (var inputStream = optionalStream.get()) {
            try (var outputStream = new ByteArrayOutputStream()) {
                IOUtils.copy(inputStream, outputStream);
                return Optional.<byte[]>of(outputStream.toByteArray());
            }
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public Optional<InputStream> getInputStream(final String path) {
        return get(this.contextBuilder, this.bucket, path, blob -> {
            try {
                return Optional.<InputStream>of(blob.getPayload().openStream());
            } catch (IOException ex) {
                throw new IllegalStateException(ex);
            }
        });
    }

    @Override
    public Optional<File> getTemporalFile(final String path) {
        Optional<Blob> optionalBlob = get(this.contextBuilder, this.bucket, path, Optional::<Blob>of);
        if (!optionalBlob.isPresent()) {
            return Optional.<File>empty();
        }
        // limpieza de nombre
        var blob = optionalBlob.get();
        var fileName = path.contains("/") ? path.split("/")[1] : path;
        var extension = FileUtils.getExtension(fileName);
        // copia archivo tmp
        try {
            var tmpFilePath = Files.createTempFile(fileName, "." + extension);
            var file = tmpFilePath.toFile();
            writeToFile(blob, file);
            return Optional.<File>of(file);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    // privados

    private static void put(final ContextBuilder contextBuilder, final String bucket,
            final Function<BlobStore, Blob> blobFunction) {
        try (var context = contextBuilder.buildView(BlobStoreContext.class)) {
            final var blobStore = context.getBlobStore();
            blobStore.createContainerInLocation(null, bucket);
            final var blob = blobFunction.apply(blobStore);
            blobStore.putBlob(bucket, blob);
        }
    }

    private static <T> Optional<T> get(final ContextBuilder contextBuilder, final String bucket, final String path,
            final Function<Blob, Optional<T>> getFunction) {

        try (var context = contextBuilder.buildView(BlobStoreContext.class)) {
            var blobStore = context.getBlobStore();
            var blob = blobStore.getBlob(bucket, path);
            if (blob == null) {
                return Optional.<T>empty();
            }
            return getFunction.apply(blob);
        }
    }

    private static void writeToFile(Blob blob, File file) throws IOException {
        try (var inputStream = blob.getPayload().openStream()) {
            try (var outputStream = new FileOutputStream(file)) {
                IOUtils.copy(inputStream, outputStream);
            }
        }
    }

}