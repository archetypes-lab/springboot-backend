package com.github.erodriguezg.springbootangular.utils.pdfutils;

import com.github.erodriguezg.springbootangular.utils.pdfutils.resources.PdfResource;

public interface PdfGenerator {

    byte[] generatePdf(byte[] xhtmlByte, PdfResource[] resources);

    byte[] generatePdf(byte[] xhtmlByte, PdfResource[] resources, PdfOptions options);

}