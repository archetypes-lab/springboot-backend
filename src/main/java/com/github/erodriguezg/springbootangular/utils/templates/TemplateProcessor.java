package com.github.erodriguezg.springbootangular.utils.templates;

public interface TemplateProcessor {
    
    /**
     * Procesa una template
     * @param template ruta de plantilla en classpath
     * @param model modelo utilizado en la plantilla
     * @return plantilla evaluada en un string
     */
    String process(String template, Object model);
    
}