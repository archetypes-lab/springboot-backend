package com.github.erodriguezg.springbootangular.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

public final class HttpUtils {

    private HttpUtils() {
        // no se puede instanciar
    }

    public static void copyToHttpResponse(File file, HttpServletResponse httpResponse) {
        try {
            copyToHttpResponse(new FileInputStream(file), httpResponse);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static void copyToHttpResponse(InputStream inputStream, HttpServletResponse httpResponse) {
        try (inputStream) {
            try (var output = httpResponse.getOutputStream()) {
                IOUtils.copy(inputStream, output);
            }
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    public static void downloadFile(HttpServletResponse httpResponse, File file, boolean deleteFile) {
        httpResponse.setHeader("Content-disposition", "attachment; filename=" + FileUtils.prettyFileName(file));
        httpResponse.setContentType(FileUtils.getContentType(file));
        httpResponse.setContentLengthLong(file.length());
        try {
            copyToHttpResponse(file, httpResponse);
        } finally {
            if (deleteFile) {
                FileUtils.deleteQuietly(file);
            }
        }
    }

}