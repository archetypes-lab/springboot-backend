package com.github.erodriguezg.springbootangular.utils.pdfutils.resources;

import lombok.Data;

@Data
public abstract class PdfResource {

    private String name;
    
    private PdfResourceType resourceType;

}