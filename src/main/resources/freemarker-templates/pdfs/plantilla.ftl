<#macro main_report tituloReporte="">
    <html>
        <head>
            <style type="text/css">
                body {
			        font-family: helvetica;
		        }
            </style>
        </head>
        <body>
            <div class="header">
                <table>
                    <tr>
                        <td style="width: 800px">
                            <h1>${tituloReporte}!</h1>
                        </td>
                        <td>
                            <img src="logo-header" style="width:100px" />
                        </td>
                    </tr>
                </table>
            </div>
            
            <div class="content">
                <#nested />
            </div>
            
            <div class="footer">
                <h3>Footer</h3>
            </div>
        </body>
    </html>
</#macro>