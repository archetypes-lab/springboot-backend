<#macro main_email tituloEmail="">
    <html>
        <head>
        </head>
        <body>
            <div class="header">
                <table>
                    <tr>
                        <td style="width: 800px">
                            <h1>${tituloEmail}</h1>
                        </td>
                        <td>
                            <img src='cid:logo.png' style="width: 130px" />
                        </td>
                    </tr>
                </table>
            </div>
            
            <div class="content">
                <#nested />
            </div>
            
            <div class="footer">
                <img src='cid:footer.png' style="width: 100%" />
            </div>
        </body>
    </html>
</#macro>