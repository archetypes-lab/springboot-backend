package com.github.erodriguezg.springbootangular;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationIT {

    @Test
    public void contextLoads() {
        var inicioSpringBoot = true;
        assertThat(inicioSpringBoot).isTrue();
    }

}
