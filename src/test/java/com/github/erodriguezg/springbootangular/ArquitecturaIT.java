package com.github.erodriguezg.springbootangular;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.github.erodriguezg.springbootangular.utils.SecurityUtils;
import com.tngtech.archunit.base.DescribedPredicate;
import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.domain.JavaMethod;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.springframework.boot.logging.LogLevel;
import org.springframework.boot.logging.LoggingSystem;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

public class ArquitecturaIT {

    private static final String BASE_PACKAGE = "com.github.erodriguezg";

    private static final String URL_VALID_REGEX = "^(\\$\\{app\\.rest\\.prefix\\})?((\\/[a-z0-9\\-]+)*(\\/\\$?\\{[a-zA-Z0-9]+\\})*)*$";

    @BeforeClass
    public static void setErrorLogging() {
        LoggingSystem.get(ClassLoader.getSystemClassLoader()).setLogLevel(Logger.ROOT_LOGGER_NAME, LogLevel.INFO);
    }

    // TEST

    // IDENTIDAD

    @Test
    public void securityUtilsShouldOnlyBeAccesedByRestControllersAndConfiguration() {
        JavaClasses importedClasses = new ClassFileImporter().importPackages(BASE_PACKAGE);
        ArchRule rule = 
            classes().that().areAssignableTo(SecurityUtils.class)
            .should().onlyBeAccessed().byClassesThat(HAVE_A_CLASS_ANNOTATED_WITH_REST_OR_CONFIG);
        rule.check(importedClasses);
    }

    // URLS

    @Test
    public void classMappingUriRegularExpressionCheck() {
        JavaClasses importedClasses = new ClassFileImporter().importPackages(BASE_PACKAGE);
        ArchRule rule = classes().that(HAVE_A_CLASS_ANNOTATED_WITH_REQUEST_MAPPING)
                .should(CLASS_REQUEST_MAPPING_SHOULD_HAVE_VALID_URIS);
        rule.check(importedClasses);
    }

    @Test
    public void requestMethodMappingUriRegularExpressionCheck() {
        JavaClasses importedClasses = new ClassFileImporter().importPackages(BASE_PACKAGE);
        ArchRule rule = methods().that(HAVE_A_METHOD_ANNOTATED_WITH_REQUEST_MAPPING)
                .should(REQUEST_METHODS_SHOULD_HAVE_VALID_URIS);
        rule.check(importedClasses);
    }

    @Test
    public void getMethodMappingUriRegularExpressionCheck() {
        JavaClasses importedClasses = new ClassFileImporter().importPackages(BASE_PACKAGE);
        ArchRule rule = methods().that(HAVE_A_METHOD_ANNOTATED_WITH_GET_MAPPING)
                .should(GET_METHODS_SHOULD_HAVE_VALID_URIS);
        rule.check(importedClasses);
    }

    @Test
    public void postMethodMappingUriRegularExpressionCheck() {
        JavaClasses importedClasses = new ClassFileImporter().importPackages(BASE_PACKAGE);
        ArchRule rule = methods().that(HAVE_A_METHOD_ANNOTATED_WITH_POST_MAPPING)
                .should(POST_METHODS_SHOULD_HAVE_VALID_URIS);
        rule.check(importedClasses);
    }

    @Test
    public void putMethodMappingUriRegularExpressionCheck() {
        JavaClasses importedClasses = new ClassFileImporter().importPackages(BASE_PACKAGE);
        ArchRule rule = methods().that(HAVE_A_METHOD_ANNOTATED_WITH_PUT_MAPPING)
                .should(PUT_METHODS_SHOULD_HAVE_VALID_URIS);
        rule.check(importedClasses);
    }

    @Test
    public void deleteMethodMappingUriRegularExpressionCheck() {
        JavaClasses importedClasses = new ClassFileImporter().importPackages(BASE_PACKAGE);
        ArchRule rule = methods().that(HAVE_A_METHOD_ANNOTATED_WITH_DELETE_MAPPING)
                .should(DELETE_METHODS_SHOULD_HAVE_VALID_URIS);
        rule.check(importedClasses);
    }

    @Test
    public void patchMethodMappingUriRegularExpressionCheck() {
        JavaClasses importedClasses = new ClassFileImporter().importPackages(BASE_PACKAGE);
        ArchRule rule = methods().that(HAVE_A_METHOD_ANNOTATED_WITH_PATCH_MAPPING)
                .should(PATCH_METHODS_SHOULD_HAVE_VALID_URIS);
        rule.check(importedClasses);
    }

    /* classes */

    private static DescribedPredicate<JavaClass> HAVE_A_CLASS_ANNOTATED_WITH_REST_OR_CONFIG = new DescribedPredicate<JavaClass>(
            "have a class annotated with any annotation") {
        @Override
        public boolean apply(JavaClass class1) {
            return class1.isAnnotatedWith(RestController.class) || class1.isAnnotatedWith(Configuration.class)
            || class1.isAssignableFrom(SecurityUtils.class);
        }
    };

    private static DescribedPredicate<JavaClass> HAVE_A_CLASS_ANNOTATED_WITH_REQUEST_MAPPING = new DescribedPredicate<JavaClass>(
            "have a method annotated with @GetMapping or @PostMapping or @DeleteMapping or @PutMapping or @PatchMapping or @RequestMapping") {

        @Override
        public boolean apply(JavaClass clazz) {
            return clazz.isAnnotatedWith(RequestMapping.class);
        }
    };

    private static ArchCondition<JavaClass> CLASS_REQUEST_MAPPING_SHOULD_HAVE_VALID_URIS = new ArchCondition<JavaClass>(
            "should have a valid uri") {
        @Override
        public void check(JavaClass clazz, ConditionEvents events) {
            Set<String> urls = new HashSet<>();
            if (clazz.getAnnotationOfType(RequestMapping.class) != null) {
                urls.addAll(Arrays.asList(clazz.getAnnotationOfType(RequestMapping.class).value()));
            }

            urls.forEach(url -> {
                if (!url.matches(URL_VALID_REGEX)) {
                    var msg = String.format("Rest URI %s of Class %s is not valid", url, clazz.getFullName());
                    events.add(SimpleConditionEvent.violated(clazz, msg));
                }

            });
        }
    };

    /* methods */

    private static void VALIDATE_METHOD_URLS(Set<String> urls, JavaMethod method, ConditionEvents events) {
        urls.forEach(url -> {
            if (!url.matches(URL_VALID_REGEX)) {
                var msg = String.format("Rest URI %s of Method %s is not valid", url, method.getFullName());
                events.add(SimpleConditionEvent.violated(method, msg));
            }
        });
    }

    // RequestMapping

    private static DescribedPredicate<JavaMethod> HAVE_A_METHOD_ANNOTATED_WITH_REQUEST_MAPPING = new DescribedPredicate<JavaMethod>(
            "have a method annotated with @GetMapping") {
        @Override
        public boolean apply(JavaMethod method) {
            return method.isAnnotatedWith(RequestMapping.class);
        }
    };

    private static ArchCondition<JavaMethod> REQUEST_METHODS_SHOULD_HAVE_VALID_URIS = new ArchCondition<JavaMethod>(
            "should have a valid uri") {
        @Override
        public void check(JavaMethod method, ConditionEvents events) {
            Set<String> urls = new HashSet<>();
            if (method.getAnnotationOfType(RequestMapping.class) != null) {
                urls.addAll(Arrays.asList(method.getAnnotationOfType(RequestMapping.class).value()));
            }
            VALIDATE_METHOD_URLS(urls, method, events);
        }
    };

    // GET

    private static DescribedPredicate<JavaMethod> HAVE_A_METHOD_ANNOTATED_WITH_GET_MAPPING = new DescribedPredicate<JavaMethod>(
            "have a method annotated with @GetMapping") {
        @Override
        public boolean apply(JavaMethod method) {
            return method.isAnnotatedWith(GetMapping.class);
        }
    };

    private static ArchCondition<JavaMethod> GET_METHODS_SHOULD_HAVE_VALID_URIS = new ArchCondition<JavaMethod>(
            "should have a valid uri") {
        @Override
        public void check(JavaMethod method, ConditionEvents events) {
            Set<String> urls = new HashSet<>();
            if (method.getAnnotationOfType(GetMapping.class) != null) {
                urls.addAll(Arrays.asList(method.getAnnotationOfType(GetMapping.class).value()));
            }
            VALIDATE_METHOD_URLS(urls, method, events);
        }
    };

    // POST

    private static DescribedPredicate<JavaMethod> HAVE_A_METHOD_ANNOTATED_WITH_POST_MAPPING = new DescribedPredicate<JavaMethod>(
            "have a method annotated with @GetMapping") {
        @Override
        public boolean apply(JavaMethod method) {
            return method.isAnnotatedWith(PostMapping.class);
        }
    };

    private static ArchCondition<JavaMethod> POST_METHODS_SHOULD_HAVE_VALID_URIS = new ArchCondition<JavaMethod>(
            "should have a valid uri") {
        @Override
        public void check(JavaMethod method, ConditionEvents events) {
            Set<String> urls = new HashSet<>();
            if (method.getAnnotationOfType(PostMapping.class) != null) {
                urls.addAll(Arrays.asList(method.getAnnotationOfType(PostMapping.class).value()));
            }
            VALIDATE_METHOD_URLS(urls, method, events);
        }
    };

    // PUT

    private static DescribedPredicate<JavaMethod> HAVE_A_METHOD_ANNOTATED_WITH_PUT_MAPPING = new DescribedPredicate<JavaMethod>(
            "have a method annotated with @GetMapping") {
        @Override
        public boolean apply(JavaMethod method) {
            return method.isAnnotatedWith(PutMapping.class);
        }
    };

    private static ArchCondition<JavaMethod> PUT_METHODS_SHOULD_HAVE_VALID_URIS = new ArchCondition<JavaMethod>(
            "should have a valid uri") {
        @Override
        public void check(JavaMethod method, ConditionEvents events) {
            Set<String> urls = new HashSet<>();
            if (method.getAnnotationOfType(PutMapping.class) != null) {
                urls.addAll(Arrays.asList(method.getAnnotationOfType(PutMapping.class).value()));
            }
            VALIDATE_METHOD_URLS(urls, method, events);
        }
    };

    // DELETE

    private static DescribedPredicate<JavaMethod> HAVE_A_METHOD_ANNOTATED_WITH_DELETE_MAPPING = new DescribedPredicate<JavaMethod>(
            "have a method annotated with @GetMapping") {
        @Override
        public boolean apply(JavaMethod method) {
            return method.isAnnotatedWith(DeleteMapping.class);
        }
    };

    private static ArchCondition<JavaMethod> DELETE_METHODS_SHOULD_HAVE_VALID_URIS = new ArchCondition<JavaMethod>(
            "should have a valid uri") {
        @Override
        public void check(JavaMethod method, ConditionEvents events) {
            Set<String> urls = new HashSet<>();
            if (method.getAnnotationOfType(DeleteMapping.class) != null) {
                urls.addAll(Arrays.asList(method.getAnnotationOfType(DeleteMapping.class).value()));
            }
            VALIDATE_METHOD_URLS(urls, method, events);
        }
    };

    // PATCH

    private static DescribedPredicate<JavaMethod> HAVE_A_METHOD_ANNOTATED_WITH_PATCH_MAPPING = new DescribedPredicate<JavaMethod>(
            "have a method annotated with @GetMapping") {
        @Override
        public boolean apply(JavaMethod method) {
            return method.isAnnotatedWith(PatchMapping.class);
        }
    };

    private static ArchCondition<JavaMethod> PATCH_METHODS_SHOULD_HAVE_VALID_URIS = new ArchCondition<JavaMethod>(
            "should have a valid uri") {
        @Override
        public void check(JavaMethod method, ConditionEvents events) {
            Set<String> urls = new HashSet<>();
            if (method.getAnnotationOfType(PatchMapping.class) != null) {
                urls.addAll(Arrays.asList(method.getAnnotationOfType(PatchMapping.class).value()));
            }
            VALIDATE_METHOD_URLS(urls, method, events);
        }
    };

}