package com.github.erodriguezg.springbootangular;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStoreContext;
import org.junit.Ignore;
import org.junit.Test;

public class S3Test {

    @Ignore("Ejemplo de consumo API S3 por medio de JClouds")
    @Test
    public void testS3Minio() throws IOException {
        String containerName = "mycontainer";
        String identity = "AKIAIOSFODNN7EXAMPLE";
        String credentials = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY";

        Properties overrides = new Properties();
        overrides.setProperty("jclouds.provider", "s3");
        overrides.setProperty("jclouds.endpoint", "http://localhost:9000");

        var contextBuilder = ContextBuilder.newBuilder("s3")
            .credentials(identity, credentials)
            .overrides(overrides);

        BlobStoreContext context = contextBuilder.buildView(BlobStoreContext.class);

        // Access the BlobStore
        var blobStore = context.getBlobStore();

        // Create a Container
        blobStore.createContainerInLocation(null, containerName);

        // Create a Blob
        byte[] payload = "hola mundo 2, update!".getBytes(StandardCharsets.UTF_8);
        var blob = blobStore.blobBuilder("prueba/test2.txt") // you can use folders via blobBuilder(folderName +
                                                             // "/sushi.jpg")
                .payload(payload)
                .contentLength(payload.length)
                .build();

        // Upload the Blob
        blobStore.putBlob(containerName, blob);

        // Don't forget to close the context when you're done!
        context.close();

        assertThat(true).isTrue();
    }

}