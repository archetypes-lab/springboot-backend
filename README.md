
# SPRING BOOT BACKEND APP


<h2 id="req-previos-container">Requerimientos previos - Instalación Remote Container</h2> 

La instalación de <a href="https://code.visualstudio.com/docs/remote/containers">remote container</a> permite desarrollar dentro de un contenedor docker utilizando visual studio code de forma remota a este. Automaticamente se monta el ambiente de desarrollo, con los requerimientos y addons necesarios, sin embargo, el rendimiento no es tan bueno como seria en una instalación normal.

Se requiere tener instalados los siguientes componentes para el desarrollo en windows 10 o en linux.
* <a href="https://hub.docker.com/editions/community/docker-ce-desktop-windows"> Docker Community Edition o Docker for Windows</a>.

<p style="color: red;font-weight: bold">Si se desarrolla en windows se recomienda tener instalado el soporte WSL 2, para mejorar el rendimiento, ya que de manera contraria se puede ver negativamente afectado. En ese caso se recomienda una instalación normal</p>

<img src="https://i.ibb.co/28Q3kMT/image-7.png" />

Para ver como instalar WSL 2 click <a href="https://docs.docker.com/docker-for-windows/wsl/">aqui</a>

***
<h2 id="req-previos-normal">Requerimientos previos - Instalación Normal (Recomendada)</h2>

La instalación normal, solamente ocupa la maquina HOST con visual studio code,
para montar el proyecto. Requiere la instalación y configuración de todos los componentes en la maquina host.

Se requiere tener instalados los siguientes componentes para el desarrollo en windows 10 o en linux.
* <a href="https://git-scm.com/downloads" >GitBash</a>. Ultima versión. 
* <a href="https://docs.aws.amazon.com/corretto/latest/corretto-11-ug/downloads-list.html">Java JDK 11 Amazon Correto</a>.
* <a href="https://code.visualstudio.com">Visual Studio Code</a>. Ultima versión.
* <a href="https://hub.docker.com/editions/community/docker-ce-desktop-windows"> Docker Community Edition o Docker for Windows</a>. Necesario para levantar los servicios docker a los cuales se conecta el backend.

### Verificación Git

Se recomienda una versión igual o superior de Git for Windows como se ve en la imagen, ya que
se pueden presentar problemas con la integración git con visual studio code.

<img src="https://i.ibb.co/hsr1hbV/image-3.png" />

### Verificación Java 

1. Descargar el instalador automático (exe en windows) de Java 11 Amazon Corretto.
2. Verificar la versión de java en consola ejecutando: "java -version", debería entregar algo asi:

<img src="https://i.ibb.co/mvZqXK2/image-1.png"/>

3. La variable JAVA_HOME debe estar apuntando al JDK de Amazon Corretto:

<img src="https://i.ibb.co/805dWHV/image-2.png" />

### Configuración Visual Studio Code

#### Configurar addons

Las addons de visual studio nos permiten transformar el editor de texto en un IDE necesario para trabajar en el proyecto.

Para instalar un addons, hacer paso 1 y 2 como se ve en la imagen y luego presionar sobre el botón install del addons.

<img src="https://i.ibb.co/JdwM4nb/image-4.png" />

Los addons a instalar son los siguientes:

* sonarsource.sonarlint-vscode
* vscjava.vscode-java-pack
* pivotal.vscode-spring-boot
* vscjava.vscode-spring-boot-dashboard
* gabrielbb.vscode-lombok
* redhat.vscode-xml

Reiniciar visual studio code, luego de instalar todos estos addons.

Si aparece este recuadro:

<img src="https://i.ibb.co/pfdBT1m/image-5.png" />

Oprimir Restart Now

***

## ESPECIFICACÓN DE SERVICIOS UTILIZADOS

La aplicación backend utiliza los siguientes servicios

* <a href="https://www.postgresql.org">Postgresql</a>
  - La base datos relaciónal utilizada, el principal servicio utilizado por la aplicación para realizar la persistencia de su estado.
* <a href="https://redis.io">Redis</a>
  - Es una base datos en memoria de rapido acceso y escalable. Utilizada en el sistema para almacenar los datos de sesión http de los usuarios.
* <a href="https://min.io">Minio</a>
  - Un object store escalable y compatible con el protocolo amazon S3. Utilizado para el almacenamiento de archivos.
* <a href="https://www.rabbitmq.com">RabbitMQ</a>
  - Un sistema de manejo de colas compatible con el protocolo <a href="https://es.wikipedia.org/wiki/Advanced_Message_Queuing_Protocol">AMQP</a>. Utilizado en el sistema para manejar colas de procesamientos asincronas. Ejemplo: envio de correos electronicos.
* <a href="https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol">SMTP</a>
  - Servicio de envio de correos electronicos. La configuración por defecto utiliza Gmail.

***

## MONTAR PROYECTO DE DESARROLLO

1) Clonar la ruta del proyecto con Git.
2) Abrir la carpeta con visual studio code.

Si se quiere montar el proyecto de forma normal:
* <a href="#req-previos-normal">Instalar requerimientos previos de instalación normal</a>
* <a href="#configurar-addons">Configurar addons de visual studio code</a>

Si se quiere montar el proyecto con remote container, realizar paso 1 y 2 de la siguiente imagen. (requiere el addons de vstudio de remote container 'ms-vscode-remote.remote-containers').

<img src="https://i.ibb.co/4p3nWS1/image-8.png" />

3) Esperar a que visual studio code termine de configurar el proyecto. 

    <img src="https://i.ibb.co/NYHtBYz/image-10.png" />


4) Levantar los servicios necesarios para el backend como se indica <a href="#servicios-backend">aqui</a>

5) Copiar el archivo src/main/resources/application.yml.template
a src/main/resources/application.yml. Fijarse de copiar el recurso y no moverlo.

6) Configurar las variables de ambiente en application.yml que sean necesarias.
(la de los servicios y el email).

    <p style="color: red; font-weight: bold">
    Nota: Si se desarrolla en contenedor se puede utilizar 'host.docker.internal' para hacer referencia al host desde el contenedor.
    </p>

7) En la siguiente imagen se visualiza, donde se encuentran los archivos del proyecto Java (A) y como ejecutar el proyecto spring boot (B)

  <img src="https://i.ibb.co/CnP4wvM/image-11.png" />


8) Una vez lanzado el modo debug, podemos visualizar la sección para controlar las variables del debuger (A), la sección para manejar el servidor y la navegación del debugger (sección B) y la sección con el log del servidor (sección C).

<img src="https://i.ibb.co/4SNYDQB/image-12.png" />

9) Backend queda en "http://localhost:8080" por defecto.

***

## <span id="servicios-backend"></span>SERVICIOS - DOCKER COMPOSE

1) Crear un directorio cualquiera y crear un archivo llamado "docker-compose.yml" dentro del directorio.

2) Copiar lo siguiente al archivo y guardar.

    ```
    postgresql: 
      restart: always 
      image: postgres:11 
      environment: 
        POSTGRES_PASSWORD: postgres 
      ports: 
        - 5432:5432

    redis:
      restart: always
      image: redis:6.0.5-alpine
      command: redis-server --requirepass mipass
      ports:
        - 6379:6379

    minio:  
      restart: always  
      image: minio/minio:RELEASE.2020-07-02T00-15-09Z  
      command: server /data  
      environment:  
        - MINIO_ACCESS_KEY=AKIAIOSFODNN7EXAMPLE 
        - MINIO_SECRET_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY 
      ports: 
        - 9000:9000

    rabbitmq:   
      restart: always   
      image: rabbitmq:3.8-management-alpine   
      environment:   
        - RABBITMQ_DEFAULT_USER=admin
        - RABBITMQ_DEFAULT_PASS=cambiame
      ports:  
        - 15672:15672
        - 5672:5672
    ```

  3) Iniciar todos los servicios. Como se indica <a href="#crear-servicios-docker-compose">aqui</a>

### COMANDOS <a href="https://docs.docker.com/compose/">DOCKER-COMPOSE</a>

Lo siguientes comandos "docker-compose" se ejecutan por consola estando en el mismo directorio donde se encuentra el archivo "docker-compose.yml".
Donde nombre-servicio puede ser: postgresql, redis, minio y rabbitmq

1) <span id="crear-servicios-docker-compose"></span>Crear todos los contenedores docker
    ```
    docker-compose up -d
    ```
2) Crear un contenedor en especifico

    ```
    docker-compose up -d <nombre-servicio>
    ```

3) Detener un contenedor especifico

    ```
    docker-compose stop <nombre-servicio>
    ```

4) Iniciar un contenedor detenido

    ```
    docker-compose start <nombre-servicio>
    ```

5) Borrar un contenedor especifico

    ```
    docker-compose stop <nombre-servicio>
    docker-compose rm <nombre-servicio>
    ```
    <p style="color: red; font-weight: bold">
    Nota: Cuando un contenedor se detiene, su contenido NO se borra. El contenido de los contenedores NO se borra al apagar o reiniciar el host. Un contenido del contenedor solo es borrado cuando se especifica que debe ser borrado. 
    </p>

### COMANDOS <a href="https://www.docker.com">DOCKER</a>

1) Para revisar los contenedores funcionando actualmente

    ```
    docker ps
    ```

2) Para revisar los contenedores funcionando actualmente y tambien los detenidos

    ```
    docker ps -a
    ```

3) Entrar a la consola de un contenedor activo
    ```
    docker exec -it <container_id> /bin/bash
    ``` 
    Algunas imagenes no tienen /bin/bash, en ese caso utilizar /bin/sh

4) Ver el log de un contenedor 
    ```
    docker logs -f --tail 200 <container_id>
    ```
### Configuración contenedor PostgreSQL

  Para configurar una nueva base datos:

    docker ps // para ver el listado de contenedores
    docker exec -it <id contenedor postgresql> /bin/bash
    // entra a la maquina docker
    root@915560d5b2ab:/# psql -U postgres
    // entra a psql
    postgres=# create database "springboot-backend";
    // para borrar
    postgres=# drop database "springboot-backend";
    // para listar
    postgres=# \l
    // para salir
    postgres=# \q

***

## ESPECIFICACIÓN RESTAPI SWAGGER

Una vez iniciado el proyecto se ubica en
```
http://localhost:8080/swagger-ui.html
```

<img src="https://i.ibb.co/p4ybVQ3/image-9.png" />

***

## SonarQube (JaCoCo: Unit Test + Integration Test + Coverage)

Para realizar al inspección con sonarqube se deben realizar los siguientes pasos:

1) Activar Plugin JaCoCo en SonarQube
2) Lanzar comando Maven:

mvn clean verify jacoco:report sonar:sonar -Dsonar.host.url=https://localhost:9000 -Dsonar.login=019d1e2e04e -Dcobertura.report.format=xml

<p style="color: red; font-weight: bold">
Nota: Este proyecto viene integrado a un pipeline CI/CD de Gitlab, donde se explica como realizar esto mismo pero integrado al pipeline.
</p>

***

## CONSEJOS AL DESARROLLAR
 * Utilizar las <a href="https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf">shortcuts</a> al desarrollar:
    * <strong>control + p</strong>: búsqueda de archivos
    * <strong>alt + shift + o</strong>: ordenar imports automaticamente
    * <strong>alt + shift +f</strong>: auto formatear código
    * <strong>ctrl + shift + k</strong>: Eliminar linea
    * <strong>alt + shift + flecha abajo</strong>: Duplicar linea
    * <strong>F2</strong>: refactorizar
